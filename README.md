# Game troubleshooting
This project is simply a collection of tricks I've learned over time to get old LAN games
working properly on Windows 10.

## Miscellanious errors (errors that apply to all games)

### Can't view LAN servers in browser
- Disable 3rd party network adapters (Hamachi, Tungle, virtual machines etc)
- Ensure network type is set to private
- Ensure firewall isn’t blocking private network features
- Check advanced firewall to unblock apps
- Try direct connect via console (example: `connect 10.0.0.13`)

### Safemode warnings
- *Almost* always, press NO. Typically they reset graphic settings