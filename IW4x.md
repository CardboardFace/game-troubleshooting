# IW4x troubleshooting

## Fatal error/black screen
Create a shortcut that runs the game using `-nosteam` in the launch arguments

## Freeze on splash screen, crash to desktop or blackscreen
- Disable all overlays (**screen dimmers**, NVidia/Shadowplay, W10 game-bar, Discord, Overwolf, Steam, FPS counters etc)
- Install redists
- Set iw4x.exe properties > compatibility > DPI to use "Application"
- Set iw4x.exe properties > compatibility > Check "Disable fullscreen optimisations"
- Quit Steam and Nvidia tray before launching
- Run as administrator 
- Try signing in and out of Windows

## Client causes server crash or disconnects on connecting to LAN
- Delete `players` folder from game dir
