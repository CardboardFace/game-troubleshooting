# GTA 4 troubleshooting

## Installing
1. Install & login to [Games For Windows Live Marketplace](https://go.microsoft.com/fwlink/?LinkID=201134)
2. Delete your game directory if modded then delete from Steam and reinstall
3. Run game once
4. Follow instructions from & install my [modpack](https://gitlab.com/CardboardFace/gta-4-mod-pack)
5. Open `GTAIV.exe` properties and enable "Disable fullscreen optimisations"
6. Disable Steam overlay for GTA4

## Rockstar account login
1. Ensure your game is on version 1.0.0.7
2. Press "Play offline" (you only need to login to LIVE for LAN)

## Can't find LAN servers (quick match)
- Ensure your game is on version 1.0.0.7
- Install & login to [Games For Windows Live Marketplace](https://go.microsoft.com/fwlink/?LinkID=201134)
- Sign into Windows Live in-game (press home)
- Remove `xlive.dll` or `xliveless.dll` from your game directory
- Ensure you haven't used the same CD key as any other player in the match. If yes, download the [revoke](https://support.rockstargames.com/articles/360001184488/Revoking-a-Grand-Theft-Auto-IV-License-on-PC) tool to change!

## Graphics stuck on lowest setting
- Ensure you have downloaded and install my modpack