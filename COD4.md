# COD 4 troubleshooting

## Unlocking all single player missions
1. Enable dev console
2. Open and run command: `seta mis_01 "20"`

## CD key & Hacking
1. Run game as admin to ensure CD key saves to sys registry
2. Input CD key
3. Run Easy Account Manager (as admin)
4. Easy upgrade for 1.7
5. Edit one class and relaunch the game
6. If your stats have saved and you can join a LAN server, you are free to edit classes.

## Screen HZ stuck at 91
1. Open dev console
2. Type `/seta com_maxfps 144; seta r_displayRefresh 144 Hz` (also enable in graphics settings)

## FOV too low
1. Open dev console
2. Type `/seta cg_fov 80` (exceeding 80 shows weird parts of the viewmodel)

## 0xc0000005 error
- Install dependencies (DirectX, .Net framework)
- Disable AV
- Run CCleaner regedit fix

## Black screen and crash to desktop
- Connect anything to the Mic aux input