@echo off
title Troubleshooting updater
cls
echo Committing changes...
git add .
git commit -m "Updated guides"
echo Pushing guide updates...
git push
echo.
echo Press any key to exit...
pause>nul