# Dead Rising 2 troubleshooting

## Crash when loading saves
- Your game directory must have the same mods installed since your last successful save. For instance, if you changed your FOV, you must restore the modded files to load a modded savegame.
- Try restarting the story. You'll keep your stats but lose your story progress.