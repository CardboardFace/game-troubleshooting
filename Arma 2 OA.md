# Arma 2 OA

## Custom FOV
Use an Arma FOV calculator to find what to set the values in `Documents\Arma 2` or `Documents\Arma 2 Other Profiles` in your .ArmA2OAProfile file.
Example site: [hia3.com/tools/armafovcalculator-en](http://hia3.com/tools/armafovcalculator-en)

## "Session lost" when connecting to LAN dedicated server
- Ensure you're launching from a Steam account with A2 OA purchased
- Launch game from within Steam library 
- Relaunch the game

## Game won't start
- Close any dedicated servers and open them after starting the game

## Endless "Waiting for host"
- Check ArmA 2 maps imported successfully into Operation Arrowhead
    - If maps like "Chernarus" aren't avaliable, relaunch Arma 2 from Steam (may take multiple attempts) and validate files

## Poor performance
- Use launch options: -noPause -noSplash
- Disable Steam overlay
